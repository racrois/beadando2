package com.citec.projecthandlersystem;

import com.sun.xml.internal.bind.v2.runtime.property.PropertyFactory;
import java.net.URL;
import static java.time.Clock.system;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import model.entities.Project;
import model.entities.Task;
import model.entities.User;
import model.repositories.ProjectRepository;
import model.repositories.TaskRepository;
import model.repositories.UserRepository;

public class FXMLController implements Initializable {

    //UserRepository userRepo = new UserRepository();
    private UserRepository userRepo;
    private ProjectRepository projectRepo;
    private TaskRepository taskRepo;

    @FXML
    TableView tableUser;

    @FXML
    TableView tableProject;

    @FXML
    TableView tableTask;

    /*@FXML
    private Label label;*/
    @FXML
    private Pane inputNewUser;
    @FXML
    private Pane inputNewProject;
    @FXML
    private Pane inputNewTask;

    @FXML
    TextField nameTextField;
    @FXML
    TextField emailTextField;

    @FXML
    TextField projectNameTextField;
    @FXML
    TextField startDateTextField;
    @FXML
    TextField endDateTextField;
    @FXML
    TextField createdByTextField;

    @FXML
    TextField taskNameTextField;
    @FXML
    TextField projectIdTextField;
    @FXML
    TextField ownerIdTextField;

    @FXML
    Button newUserAddButton;
    @FXML
    Button newUserSaveButton;

    @FXML
    Button newProjectAddButton;
    @FXML
    Button newProjectSaveButton;

    @FXML
    Button newTaskAddButton;
    @FXML
    Button newTaskSaveButton;

    private final ObservableList<User> dataUser = FXCollections.observableArrayList( //new User("Oroszi Zoli", "orosziz@gmail.com")
            );

    private final ObservableList<Project> dataProject = FXCollections.observableArrayList( //new Project("Test", LocalDate.parse("2020-01-04"), LocalDate.parse("2020-09-04"), 8)
            //new Project("Test","2020-01-04","2020-09-04",8)
            );

    private final ObservableList<Task> dataTask = FXCollections.observableArrayList( //new Task("Test design", 1, 10)
            );

    @FXML
    private void handleAddNewUser(ActionEvent event) {
        tableUser.setOpacity(0);
        newUserAddButton.setOpacity(0);
        inputNewUser.setVisible(true);
    }

    @FXML
    private void handleAddNewProject(ActionEvent event) {
        tableProject.setOpacity(0);
        newProjectAddButton.setOpacity(0);
        inputNewProject.setVisible(true);
    }

    @FXML
    private void handleAddNewTask(ActionEvent event) {
        tableTask.setOpacity(0);
        newTaskAddButton.setOpacity(0);
        inputNewTask.setVisible(true);
    }

    /*@FXML
    private void handleSaveButtonAction(ActionEvent event) {
        if (nameTextField.getText().length() > 0 && emailTextField.getText().length() > 0) {
            User user = new User(nameTextField.getText(), emailTextField.getText());
            data.add(user);
            userRepo.insertUser(user);
            nameTextField.clear();
            emailTextField.clear();
            table.setOpacity(1);
            newUserAddButton.setOpacity(1);
            inputNewUser.setVisible(false);
        }
    }*/
    @FXML
    private void handleUserSaveButtonAction(ActionEvent event) {
        if (nameTextField.getText().length() > 0 && emailTextField.getText().length() > 0) {
            User user = new User(nameTextField.getText(), emailTextField.getText());
            dataUser.add(user);
            userRepo.insertUser(user);
            nameTextField.clear();
            emailTextField.clear();
            tableUser.setOpacity(1);
            newUserAddButton.setOpacity(1);
            inputNewUser.setVisible(false);
        }
    }

    //LocalDate localStartDate = LocalDate.parse(resultSet.getDate("start_date").toString());
    //java.sql.Date.valueOf(project.getStartDate())
    @FXML
    private void handleProjectSaveButtonAction(ActionEvent event) {
       
        /*System.out.println(startDateTextField.getText());
        System.out.println(startDateTextField.getText().toString());
        
        System.out.println(endDateTextField.getText());
        System.out.println(endDateTextField.getText().toString());*/
        
        //LocalDate start = LocalDate.parse("2010-01-01");
        //LocalDate end = LocalDate.parse("2011-01-01");
        //Integer created=2;
        
        //Project project = new Project("valami", start, end, created);
        
        LocalDate start = LocalDate.parse(startDateTextField.getText().toString());
        LocalDate end = LocalDate.parse(endDateTextField.getText().toString());
        Integer created = Integer.parseInt(createdByTextField.getText());

        //Project project = new Project(projectNameTextField.getText(), LocalDate.parse(startDateTextField.getText()), LocalDate.parse(endDateTextField.getText()), Integer.parseInt(createdByTextField.getText()));
        //Project project = new Project(projectNameTextField.getText(), startDateTextField.getText(), endDateTextField.getText(), createdByTextField.getText());
        Project project = new Project(projectNameTextField.getText(), start, end, created);

        dataProject.add(project);
        projectRepo.insertProject(project);

        tableProject.setOpacity(1);
        newProjectAddButton.setOpacity(1);
        inputNewProject.setVisible(false);
    }

    @FXML
    private void handleTaskSaveButtonAction(ActionEvent event) {
        Task task = new Task(taskNameTextField.getText(), Integer.parseInt(projectIdTextField.getText()), Integer.parseInt(ownerIdTextField.getText()));
        dataTask.add(task);
        taskRepo.insertTask(task);

        tableTask.setOpacity(1);
        newTaskAddButton.setOpacity(1);
        inputNewTask.setVisible(false);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {
        userRepo = new UserRepository();
        userRepo.findAll().forEach(System.out::println);
        setUserTableData();

        projectRepo = new ProjectRepository();
        projectRepo.findAll().forEach(System.out::println);
        setProjectTableData();

        taskRepo = new TaskRepository();
        taskRepo.findAll().forEach(System.out::println);
        setTaskTableData();
    }

    private void setUserTableData() {

        /*TableColumn idCol = new TableColumn("id");
        idCol.setMinWidth(100);
        idCol.setCellFactory(TextFieldTableCell.forTableColumn());
        idCol.setCellValueFactory(new PropertyValueFactory<User, String>("id"));

        idCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<User, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<User, String> event) {
                User user = event.getTableView().getItems().get(event.getTablePosition().getRow());
                user.setId(event.getNewValue());
            }
        });*/
        TableColumn nameCol = new TableColumn("name");
        nameCol.setMinWidth(150);
        nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        nameCol.setCellValueFactory(new PropertyValueFactory<User, String>("name"));

        nameCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<User, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<User, String> event) {
                User user = event.getTableView().getItems().get(event.getTablePosition().getRow());
                user.setName(event.getNewValue());
            }
        });

        TableColumn emailCol = new TableColumn("email");
        emailCol.setMinWidth(200);
        emailCol.setCellFactory(TextFieldTableCell.forTableColumn());
        emailCol.setCellValueFactory(new PropertyValueFactory<User, String>("email"));

        emailCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<User, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<User, String> event) {
                User user = event.getTableView().getItems().get(event.getTablePosition().getRow());
                user.setEmail(event.getNewValue());
            }
        });

        //table.getColumns().addAll(idCol, nameCol, emailCol);
        tableUser.getColumns().addAll(nameCol, emailCol);
        dataUser.addAll(userRepo.findAll());
        tableUser.setItems(dataUser);

    }

    private void setProjectTableData() {

        /*TableColumn idCol = new TableColumn("id");
        idCol.setMinWidth(100);
        idCol.setCellFactory(TextFieldTableCell.forTableColumn());
        idCol.setCellValueFactory(new PropertyValueFactory<User, String>("id"));

        idCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<User, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<User, String> event) {
                User user = event.getTableView().getItems().get(event.getTablePosition().getRow());
                user.setId(event.getNewValue());
            }
        });*/
        TableColumn projectNameCol = new TableColumn("projectName");
        projectNameCol.setMinWidth(200);
        projectNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        projectNameCol.setCellValueFactory(new PropertyValueFactory<Project, String>("projectName"));

        projectNameCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Project, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Project, String> event) {
                Project project = event.getTableView().getItems().get(event.getTablePosition().getRow());
                project.setProjectName(event.getNewValue());
                //project.setProjectName("project1");
            }
        });

        //LocalDate localStartDate = LocalDate.parse(resultSet.getDate("start_date").toString());
        //java.sql.Date.valueOf(project.getStartDate())
        /* TableColumn projectStartDateCol = new TableColumn("start_date");
        projectStartDateCol.setMinWidth(100);
        projectStartDateCol.setCellFactory(TextFieldTableCell.forTableColumn());
        //projectStartDateCol.setCellValueFactory(new PropertyValueFactory<Project, String>(LocalDate.parse("start_date").toString()));
        // projectStartDateCol.setCellValueFactory(new PropertyValueFactory<Project, LocalDate>(LocalDate.parse("startDate").toString()));
        projectStartDateCol.setCellValueFactory(new PropertyValueFactory<Project, String>("start_date"));

        projectStartDateCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Project, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Project, String> event) {
                Project project = event.getTableView().getItems().get(event.getTablePosition().getRow());
                //project.setStartDate(LocalDate.parse(event.getNewValue().toString()));
                //project.setStartDate(LocalDate.parse(event.getNewValue()));
                // LocalDate.parse(project.setStartDate(event.getNewValue()));
                project.setStartDate(LocalDate.parse(event.getNewValue()));
            }
        });*/
        TableColumn projectStartDateCol = new TableColumn("start_date");
        projectStartDateCol.setCellValueFactory(new PropertyValueFactory<Project, LocalDate>("startDate"));

        projectStartDateCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Project, LocalDate>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Project, LocalDate> event) {
                Project project = event.getTableView().getItems().get(event.getTablePosition().getRow());
                project.setStartDate(event.getNewValue());
            }
        });

        TableColumn projectEndDateCol = new TableColumn("end_date");
        projectEndDateCol.setCellValueFactory(new PropertyValueFactory<Project, LocalDate>("endDate"));

        projectEndDateCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Project, LocalDate>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Project, LocalDate> event) {
                Project project = event.getTableView().getItems().get(event.getTablePosition().getRow());
                project.setEndDate(event.getNewValue());
            }
        });


        /*TableColumn projectEndDateCol = new TableColumn("end_date");
        projectEndDateCol.setMinWidth(100);
        projectEndDateCol.setCellFactory(TextFieldTableCell.forTableColumn());
        projectEndDateCol.setCellValueFactory(new PropertyValueFactory<Project, String>("end_date"));

        projectEndDateCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Project, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Project, String> event) {
                Project project = event.getTableView().getItems().get(event.getTablePosition().getRow());
                //project.setEndDate(LocalDate.parse(event.getNewValue().toString()));
                project.setEndDate(LocalDate.parse(event.getNewValue()));
            }
        });*/

 /*  TableColumn projectCreatedByCol = new TableColumn("created_by");
        projectCreatedByCol.setMinWidth(100);
        projectCreatedByCol.setCellFactory(TextFieldTableCell.forTableColumn());
        //String s =String.valueOf("createdBy");
        //projectCreatedByCol.setCellValueFactory(new PropertyValueFactory<Project, String>(String.valueOf("createdBy")));
        projectCreatedByCol.setCellValueFactory(new PropertyValueFactory<Project, String>("created_by"));

        projectCreatedByCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Project, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Project, String> event) {
                Project project = event.getTableView().getItems().get(event.getTablePosition().getRow());
                //project.setCreatedBy(event.getNewValue());
                //project.setCreatedBy(Integer.parseInt(event.getNewValue()));
                //project.setCreatedBy(String.valueOf(event.getNewValue()));
            }
        });*/
        TableColumn projectCreatedByCol = new TableColumn("created_by");
        projectCreatedByCol.setCellValueFactory(new PropertyValueFactory<Project, Integer>("createdBy"));

        projectCreatedByCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Project, Integer>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Project, Integer> event) {
                Project project = event.getTableView().getItems().get(event.getTablePosition().getRow());
                project.setCreatedBy(event.getNewValue());
            }
        });

        //table.getColumns().addAll(idCol, nameCol, emailCol);
        tableProject.getColumns().addAll(projectNameCol, projectStartDateCol, projectEndDateCol, projectCreatedByCol);
        dataProject.addAll(projectRepo.findAll());
        tableProject.setItems(dataProject);
    }

    private void setTaskTableData() {

        TableColumn taskNameCol = new TableColumn("taskName");
        taskNameCol.setMinWidth(150);
        taskNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        taskNameCol.setCellValueFactory(new PropertyValueFactory<Task, String>("taskName"));

        taskNameCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Task, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Task, String> event) {
                Task task = event.getTableView().getItems().get(event.getTablePosition().getRow());
                task.setTaskName(event.getNewValue());
            }
        });

        TableColumn taskProjectIdCol = new TableColumn("project_id");
        taskProjectIdCol.setCellValueFactory(new PropertyValueFactory<Task, Integer>("projectId"));

        taskProjectIdCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Task, Integer>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Task, Integer> event) {
                Task task = event.getTableView().getItems().get(event.getTablePosition().getRow());
                task.setProjectId(event.getNewValue());
            }
        });

        TableColumn taskOwnerIdCol = new TableColumn("owner_id");
        taskOwnerIdCol.setCellValueFactory(new PropertyValueFactory<Task, Integer>("ownerId"));

        taskOwnerIdCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Task, Integer>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Task, Integer> event) {
                Task task = event.getTableView().getItems().get(event.getTablePosition().getRow());
                task.setProjectId(event.getNewValue());
            }
        });

        tableTask.getColumns().addAll(taskNameCol, taskProjectIdCol, taskOwnerIdCol);
        dataTask.addAll(taskRepo.findAll());
        tableTask.setItems(dataTask);
    }
}

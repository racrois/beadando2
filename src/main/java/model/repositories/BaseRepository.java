/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import model.entities.User;

/**
 *
 * @author User
 */
public class BaseRepository {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/projectplanner";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";

    protected Connection conn;

    public BaseRepository() {
        initDatabase();
    }

    private void initDatabase() {
        try {
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            System.out.println("Az adatbázis kapcsolat létrejött!");
        } catch (SQLException e) {
            System.err.println("Hiba az adatbázis kapcsolat létrehozása közben! " + e);
        }
    }

    protected <T> List<T> queryList(String sqlQuery, Function<ResultSet, T> mapper) throws SQLException {

        List<T> result = new ArrayList<>();

        Statement stmt = conn.createStatement();
        ResultSet resultSet = stmt.executeQuery(sqlQuery);

        while (resultSet.next()) {
            result.add(mapper.apply(resultSet));
        }

        return result;

    }

    public void closeConnection() {
        try {
            if (conn != null) {
                conn.close();
            } else {
                System.err.println("Nem is volt nyitva!");
            }
            System.out.println("Ha volt kapcsolat, az le lett zárva!");
        } catch (SQLException ex) {
            System.err.println("Hiba a kapcsolat lezárásban! " + ex);
        }
    }

}

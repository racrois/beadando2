/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.repositories.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;
import model.entities.Task;
import model.entities.User;

/**
 *
 * @author User
 */
public class TaskMapper implements Function<ResultSet, Task> {

    @Override
    public Task apply(ResultSet resultSet) {

        Task task = new Task();
        try {
            task.setId(resultSet.getInt("id"));
            task.setTaskName(resultSet.getString("task_name"));
            task.setProjectId(resultSet.getInt("project_id"));
            task.setOwnerId(resultSet.getInt("owner_id"));

            return task;

        } catch (SQLException e) {
            System.out.println("Hiba a Task létrehozás közben");
            e.printStackTrace();
        }
        return null;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.repositories.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.function.Function;
import model.entities.Project;

/**
 *
 * @author User
 */
public class ProjectMapper implements Function<ResultSet, Project> {

    @Override
    public Project apply(ResultSet resultSet) {

        Project project = new Project();
        try {
            project.setId(resultSet.getInt("id"));
            project.setProjectName(resultSet.getString("project_name"));
            LocalDate localStartDate = LocalDate.parse(resultSet.getDate("start_date").toString());
            project.setStartDate(localStartDate);
            LocalDate localEndDate = LocalDate.parse(resultSet.getDate("end_date").toString());
            project.setEndDate(localEndDate);
            project.setCreatedBy(resultSet.getInt("created_by"));

            return project;

        } catch (SQLException e) {
            System.out.println("Hiba a Project létrehozás közben");
            e.printStackTrace();
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.repositories.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;
import model.entities.User;

/**
 *
 * @author User
 */
public class UserMapper implements Function<ResultSet, User> {

    @Override
    public User apply(ResultSet resultSet) {

        User user = new User();
        try {
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setEmail(resultSet.getString("email"));

            return user;

        } catch (SQLException e) {
            System.out.println("Hiba a User létrehozás közben");
            e.printStackTrace();
        }
        return null;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.entities.Task;
import model.entities.User;
import model.repositories.mapper.TaskMapper;
import model.repositories.mapper.UserMapper;

/**
 *
 * @author User
 */
public class TaskRepository extends BaseRepository {

    private TaskMapper mapper = new TaskMapper();

    public TaskRepository() {
        super();
    }

    public List<Task> findAll() {
        List<Task> result = new ArrayList<>();
        String query = "SELECT * FROM task";

        try {

            result = queryList(query, mapper);

        } catch (SQLException e) {
            System.err.println("Hiba a lekérdezésben [findAll] " + e);
        }
        return result;
    }

    public Integer insertTask(Task task) {
        Integer id = null;
        String query = "insert into " + "task(task_name, project_id, owner_id) " + "values(?,?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, task.getTaskName());
            pstmt.setInt(2, task.getProjectId());
            pstmt.setInt(3, task.getOwnerId());
            pstmt.execute();
            ResultSet rs = pstmt.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } catch (SQLException e) {
            System.err.println("Hiba a lekérdezésben [insertTask] " + e);
        }
        return id;
    }
}

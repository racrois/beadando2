/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.entities.Project;
import model.repositories.mapper.ProjectMapper;


/**
 *
 * @author User
 */
public class ProjectRepository extends BaseRepository {

    private ProjectMapper mapper = new ProjectMapper();

    public ProjectRepository() {
        super();
    }

    public List<Project> findAll() {
        List<Project> result = new ArrayList<>();
        String query = "SELECT * FROM project";

        try {

            result = queryList(query, mapper);

        } catch (SQLException e) {
            System.err.println("Hiba a lekérdezésben [findAll] " + e);
        }
        return result;
    }

    public Integer insertProject(Project project) {
        Integer id = null;
        String query = "insert into " + "project(project_name, start_date, end_date, created_by) " + "values(?,?,?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, project.getProjectName());
            pstmt.setDate(2, java.sql.Date.valueOf(project.getStartDate()));
            pstmt.setDate(3, java.sql.Date.valueOf(project.getEndDate()));
            pstmt.setInt(4, project.getCreatedBy());
            pstmt.execute();
            ResultSet rs = pstmt.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } catch (SQLException e) {
            System.err.println("Hiba a lekérdezésben [insertProject] " + e);
        }
        return id;
    }
}

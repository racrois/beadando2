/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.repositories.mapper.UserMapper;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.entities.User;

/**
 *
 * @author User
 */
public class UserRepository extends BaseRepository {

    private UserMapper mapper = new UserMapper();

    public UserRepository() {
        super();
    }

    public List<User> findAll() {
        List<User> result = new ArrayList<>();
        String query = "SELECT * FROM user";

        try {

            result = queryList(query, mapper);

        } catch (SQLException e) {
            System.err.println("Hiba a lekérdezésben [findAll] " + e);
        }
        return result;
    }

    public Integer insertUser(User user) {
        Integer id = null;
        String query = "insert into " + "user(name, email) " + "values(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, user.getName());
            pstmt.setString(2, user.getEmail());
            pstmt.execute();
            ResultSet rs = pstmt.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } catch (SQLException e) {
            System.err.println("Hiba a lekérdezésben [insertUser] " + e);
        }
        return id;
    }

}

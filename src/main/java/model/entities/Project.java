/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import static com.sun.java.accessibility.util.EventID.KEY;
import java.time.LocalDate;
import static jdk.nashorn.internal.runtime.Debug.id;
import static jdk.nashorn.internal.runtime.JSType.NULL;

/**
 *
 * @author User
 */
public class Project {

    private Integer id;
    private String projectName;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer createdBy;

    public Project(Integer id, String projectName, LocalDate startDate, LocalDate endDate, Integer createdBy) {
        this.id = id;
        this.projectName = projectName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdBy = createdBy;
    }

    public Project(String projectName, LocalDate startDate, LocalDate endDate, Integer createdBy) {
        this.projectName = projectName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdBy = createdBy;
    }

    public Project() {
    }

    public Integer getId() {
        return id;
    }

    public String getProjectName() {
        return projectName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "Project{" + "id=" + id + ", projectName=" + projectName + ", startDate=" + startDate + ", endDate=" + endDate + ", createdBy=" + createdBy + '}';
    }

}

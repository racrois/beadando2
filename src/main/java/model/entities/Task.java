/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

/**
 *
 * @author User
 */
public class Task {

    private Integer id;
    private String taskName;
    private Integer projectId;
    private Integer ownerId;

    public Task(Integer id, String taskName, Integer projectId, Integer ownerId) {
        this.id = id;
        this.taskName = taskName;
        this.projectId = projectId;
        this.ownerId = ownerId;
    }

    public Task(String taskName, Integer projectId, Integer ownerId) {
        this.taskName = taskName;
        this.projectId = projectId;
        this.ownerId = ownerId;
    }

    public Task() {
    }

    public Integer getId() {
        return id;
    }

    public String getTaskName() {
        return taskName;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public String toString() {
        return "Task{" + "id=" + id + ", taskName=" + taskName + ", projectId=" + projectId + ", ownerId=" + ownerId + '}';
    }

}

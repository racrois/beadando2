-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2021. Aug 02. 00:02
-- Kiszolgáló verziója: 10.4.19-MariaDB
-- PHP verzió: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `projectplanner`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `project_name` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `project`
--

INSERT INTO `project` (`id`, `project_name`, `start_date`, `end_date`, `created_by`) VALUES
(1, 'System Design', '2010-01-01', '2020-01-02', 1),
(2, 'System Development', '2020-01-01', '2021-02-02', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `task_name` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `task`
--

INSERT INTO `task` (`id`, `task_name`, `project_id`, `owner_id`) VALUES
(1, 'Tűzoltás', 1, 3),
(2, 'Gyerek vigyázás', 2, 7);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `name`, `email`) VALUES
(1, 'Nagy Péter', 'nagy.peter@gmail.com'),
(2, 'Kis Teréz', 'kis.terez@gmail.com'),
(3, 'Nagy Rita', 'nagyrita@gmail.com'),
(4, 'Kis Pál', 'kispal@gmail.com'),
(5, 'Kis Pisti', 'kisoisti@gmail.com'),
(6, 'Pákozdi Péter', 'ppeter@gmail.com'),
(7, 'Alföldi Péter', 'apeter@gmail.com'),
(8, 'Nagy Géza', 'ng@gmail.com'),
(9, 'Gyenes Nándi', 'gyn@gmail.com'),
(10, 'Nagy Olga', 'nolga@gmail.com'),
(11, 'Nagy Laci', 'nagy.laci@gmail.com'),
(12, 'Nagy Gergő', 'nagy.gergo@gmail.com'),
(13, 'Nagy Bálint', 'nagy.balint@gmail.com'),
(14, 'Nagy Imre', 'nagy.imre@gmail.com'),
(15, 'Nagy Károly', 'nagy.karoly@gmail.com'),
(16, 'Nagy Eszter', 'ne@gmail.com'),
(17, 'Kincses István', 'ki@gmail.com'),
(18, 'Kincses Árpi', 'ka@gmail.com'),
(19, 'Nagy Hedvig', 'nh@gmail.com'),
(20, 'Nagy Tibi', 'nt@gmail.com'),
(21, 'Nagy Tamás', 'nt@gmail.com'),
(22, 'Kincses Ferenc', 'kf@gmail.com'),
(23, 'Lilla', 'lilla@gmail.com'),
(24, 'Réka', 'reka@gmail.com'),
(25, 'Péter', 'peter@gmail.com'),
(26, 'Nóri', 'nori@gmail.com'),
(27, 'Andi', 'andi@gmail.com');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

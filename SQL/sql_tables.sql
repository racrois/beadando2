CREATE TABLE User (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(100), 
  email varchar(50)
);

CREATE TABLE Project (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  project_name varchar(100),
  start_date date,
  end_date date,
  created_by int
);

CREATE TABLE Task (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  task_name varchar(100),
  project_id int,
  owner_id int
);